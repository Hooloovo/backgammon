function parseChecker(checker) {
    return checker - 1;
}

export const parseBoard = (positions) => {
    return positions.reduce((acc, elem) => {
        return acc.concat([[elem.count, parseChecker(elem.player)]])        
    }, []).reverse();
}

export const parseMoves = (moves) => {
    let resultMoves = {};
    // .filter(({ move }) => move.to_position !== null)
    moves
    .map(({ move: elem }) => ({
        ...elem,
        from_position: 24 - elem.from_position,
        to_position: elem.to_position !== null ? 24 - elem.to_position : null,
    }))
    .forEach(element => {
        console.log(element.from_position);
        if (resultMoves[`p${element.from_position}`] === undefined) {
            resultMoves[`p${element.from_position}`] = [element.dice]
        } else {
            resultMoves[`p${element.from_position}`].push(element.dice)
        }
    });

    console.log(resultMoves)

    return resultMoves;
}

export const getNextMoves = (start, target, availableMoves, dice) => {
    for (let i = 0; i < availableMoves.length; ++i) {
        let move = availableMoves[i].move;
        let next = availableMoves[i].next;
        if (move.from_position + start === 24) {
            if (target > 0 && move.to_position + target === 24) {
                return next;
            } else if (target <= 0 && move.to_position === null && dice === move.dice) {
                return next;
            }
        }
    }
    return [];
}
