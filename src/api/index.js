import axios from 'axios';


const api = 'https://glacial-plateau-28108.herokuapp.com';

export const startGame = (name) => axios.get(`${api}/start/${name}`)
    .then(({ data }) => data);

export const moveApi = (id, move) => axios.post(`${api}/move/${id}`, move)
    .then(({ data }) => data);

