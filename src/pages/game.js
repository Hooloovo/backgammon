import React, { useRef, useEffect, useState } from 'react';
import { init } from '../logic';
import './style.css';


export const Game = () => {
    const [history, setHistory] = useState([]);
    const canvasRef = useRef();
    const undoRef = useRef();
    const resetRef = useRef();

    function pushMove(player, from, to, cubes) {
        setHistory(oldHistory => [...oldHistory, {
            player,
            from,
            to,
            cubes
        }]);
    }

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");

        init(canvas, ctx, undoRef.current, resetRef.current, pushMove)
    }, []);

    useEffect(() => {
        console.log('history', history);
    }, [history])

    return (
        <>
            <canvas id="myCanvas" ref={canvasRef}></canvas>
            <div className="historyContainer">
                <span className="historyLine">История ходов:</span>
                {history.reduce((acc, elem, i, arr) => {
                    if (acc.length === 0) {
                        return [[elem]]
                    } else if (acc[acc.length - 1][0].player === elem.player) {
                        acc[acc.length - 1].push(elem);
                    } else if (acc[acc.length - 1][0].player !== elem.player) {
                        acc.push([elem]);
                    }

                    return acc;
                }, []).map((elem, i) =>
                    <span>
                        <h4>{i + 1} ход {elem[0].player === 1 ? 'черных ' : 'белых '}</h4>
                        {elem.map(move => 
                            <span>
                                С позиции {move.from + 1} на позицию {move.to + 1}<br />
                            </span>)}
                        <br />
                    </span>
                )}
            </div>
            <button id="undo" ref={undoRef}>undo</button>
            <button id="reset" ref={resetRef}>reset</button>
        </>
    )
}
